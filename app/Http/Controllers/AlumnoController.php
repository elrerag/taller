<?php

namespace App\Http\Controllers;

use App\Modelos\Alumno;
use Illuminate\Http\Request;

class AlumnoController extends Controller
{
    //
    public function index()
    {
        $alumnos = Alumno::paginate(10);
        return view('alumno-listar', compact('alumnos'));
    }

    public function saludo($variable)
    {
        return 'saludo = '.$variable;
    }
}
