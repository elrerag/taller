<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    //
    protected $table = 'curso';

    // |curso| >-< |alumno|
    public function cursos()
    {
        return $this->belongsToMany(
            Alumno::class,
            'alumno_tiene_curso',
            'curso_id',
            'alumno_id'
        );
    }
}
