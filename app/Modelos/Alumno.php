<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    //
    protected $table = 'alumno';

    // |alumno| -< |nota|
    public function notas()
    {
        return $this->hasMany(Nota::class, 'alumno_id', 'id');
    }

    // |alumno| >-< |curso|
    public function cursos()
    {
        return $this->belongsToMany(
            Curso::class,
            'alumno_tiene_curso',
            'alumno_id',
            'curso_id'
        );
    }
}
