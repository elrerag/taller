<?php

use Illuminate\Database\Seeder;

use App\Modelos\Alumno;

class AlumnoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create('es_ES');

        for($i=0; $i<100; $i++)
        {
            Alumno::create([
                'nombre' => $faker->name(),
            ]);
        }

    }
}
