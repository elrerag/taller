<?php

use Illuminate\Database\Seeder;

use App\Modelos\Curso;

class CursoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create('es_ES');

        for($i=0; $i<10; $i++)
        {
            Curso::create([
                'nombre' => $faker->word(),
            ]);
        }
    }
}
