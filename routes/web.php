<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/hello', function() {
    return 'Hello World';
});

Route::get('user/{id}', function($id)
{
    return 'User '.$id;
});

//optional param
Route::get('opcional/{id?}', function($id = null)
{
    return 'Variable = '.$id;
});

//default values
Route::get('nombre/{name?}', function($name = 'John')
{
    return 'Nombre = '.$name;
});

// Controllers

Route::get('/alumno', 'AlumnoController@index')->name('alumno');


Route::get('/saludo/{variable}', 'AlumnoController@saludo' );


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
