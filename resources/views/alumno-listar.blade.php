@extends('layouts.app')


@section('content')
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th>Nombre del alumno</th>
            </tr>
            </thead>
            <tbody>
            @foreach($alumnos as $alumno)
            <tr>
                <td>{{$alumno->nombre}}</td>

            </tr>
            @endforeach
            </tbody>
        </table>
        {{ $alumnos->links() }}
    </div>
@endsection